package dns;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NameDecoder {
    private static final Logger log = LoggerFactory.getLogger(NameDecoder.class);

    public static DecodedName decode(byte[] bytes, int offset, byte[] message) {
        StringBuffer sb = new StringBuffer();
        int i = 0;
        for (i = offset; i < bytes.length; i++) {
            byte charsOrPointer = bytes[i];
            if ((charsOrPointer & 0b11000000) != 0) {
                short pointer = 0;
                pointer |= ((bytes[i++] & 0b00111111) << 8);
                pointer |= (bytes[i] & 0xFF);
                log.debug("Hmm... looks like a compression pointer to offset " + pointer);
                if (sb.length() > 0) {
                    sb.append(".");
                }
                sb.append(NameDecoder.decode(message, pointer, message).name());

                // a pointer ends the name
                break;
            } else {
                // no compression
                int chars = charsOrPointer;
                if (chars > 0) {
                    if (i>offset) {
                        sb.append(".");
                    }
                    sb.append(new String(bytes, i+1, chars));
                    i+= chars;
                } else {
                    break;
                }
            }
        }
        int length = i - offset + 1;
        log.debug("Decoded name " + sb.toString() + " starting at offset " + offset + " with length " + length);
        return new DecodedName(sb.toString(),length);
    }
}
