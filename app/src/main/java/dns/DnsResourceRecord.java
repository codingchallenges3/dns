package dns;

import java.util.Arrays;
import java.util.HexFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DnsResourceRecord implements DnsMessagePart {
    private static final Logger log = LoggerFactory.getLogger(DnsResourceRecord.class);
    
    private String name;
    private short type;
    private short clazz;
    private int ttl;
    private short rdlength;
    private byte[] rdata;

    private int length;

    private DnsMessage parent;

    public DnsResourceRecord(DnsMessage parent, byte[] bytes, int offset) {
        this.parent = parent;
        int parseOffset = offset;

        log.debug("First 5 bytes: " + HexFormat.of().formatHex(bytes, parseOffset, parseOffset + 5));

        DecodedName dn = NameDecoder.decode(bytes, parseOffset, bytes);
        this.name = dn.name();
        parseOffset += dn.length();

        log.debug("Parsing type at offset " + parseOffset);
        this.type |= (bytes[parseOffset++] << 8);
        this.type |= bytes[parseOffset++];
        log.debug("parsed type " + type);

        log.debug("Parsing class at offset " + parseOffset);
        this.clazz |= (bytes[parseOffset++] << 8);
        this.clazz |= bytes[parseOffset++];
        log.debug("Parsed class: " + clazz);

        log.debug("ttl 4 bytes: " + HexFormat.of().formatHex(bytes, parseOffset, parseOffset + 4));
        this.ttl |= (bytes[parseOffset++] & 0xFF) << 24;
        this.ttl |= (bytes[parseOffset++] & 0xFF) << 16;
        this.ttl |= (bytes[parseOffset++] & 0xFF) << 8;
        this.ttl |= (bytes[parseOffset++] & 0xFF);
        log.debug("Parsed ttl: " + ttl);

        this.rdlength |= (bytes[parseOffset++] & 0xFF) << 8;
        this.rdlength |= (bytes[parseOffset++] & 0xFF);
        log.debug("Parsed rdlength: " + rdlength);

        this.rdata = new byte[rdlength];
        for (int i = 0; i < rdlength; i++) {
            rdata[i] = bytes[parseOffset++];            
        }
        log.debug("Parsed rdata: " + Arrays.toString(rdata));

        this.length = parseOffset - offset;
    }

    @Override
    public int length() {
        return length;
    }

    @Override
    public String toBytesString() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'toBytesString'");
    }

    @Override
    public String toString() {
        return "DnsResourceRecord [name=" + name + ", type=" + type + ", clazz=" + clazz + ", ttl=" + ttl
                + ", rdlength=" + rdlength + ", rdata=" + Arrays.toString(rdata) + ", length=" + length + "]";
    }

    public static Logger getLog() {
        return log;
    }

    public String getName() {
        return name;
    }

    public short getType() {
        return type;
    }

    public short getClazz() {
        return clazz;
    }

    public int getTtl() {
        return ttl;
    }

    public short getRdlength() {
        return rdlength;
    }

    public byte[] getRdata() {
        return rdata;
    }

    public int getLength() {
        return length;
    }

    public byte[] getIpAddress() {
        if (getType() == 1) {
            return getRdata();
        }
        log.error("Can't get IP address for type " + getType() );
        return null;
    }

    public String getDomainName() {
        if (getType() == 2 || getType() == 5) {
            return NameDecoder.decode(getRdata(), 0, parent.getData()).name();
        }
        log.error("Can't get domain name for type " + getType() );
        return null;
    }
}
