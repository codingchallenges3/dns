package dns;

import java.util.HexFormat;

public class DnsHeader implements DnsMessagePart {
    private short id;
    private byte qr, opcode, aa, tc, rd, ra, z, rcode, qdcount, ancount, nscount, arcount;

    public DnsHeader(byte[] bytes, int offset) {
        // Parse the header
        id |= (bytes[0] << 8);
        id |= bytes[1];
        qr = (byte) ((bytes[2] & 0b10000000) >>> 7);
        opcode = (byte) ((bytes[2] & 0b01111000) >>> 3);
        aa = (byte) ((bytes[2] & 0b00000100) >>> 2);
        tc = (byte) ((bytes[2] & 0b00000010) >>> 1);
        rd = (byte) ((bytes[2] & 0b00000001));

        ra = (byte) ((bytes[3] & 0b10000000) >>> 7);
        z = (byte) ((bytes[3] & 0b01110000) >>> 4);
        rcode = (byte) ((bytes[3] & 0b00001111));

        qdcount |= (bytes[4] << 8);
        qdcount |= bytes[5];
        ancount |= (bytes[6] << 8);
        ancount |= bytes[7];
        nscount |= (bytes[8] << 8);
        nscount |= bytes[9];
        arcount |= (bytes[10] << 8);
        arcount |= bytes[11];
    }

    public DnsHeader(short id,
                     byte qr, byte opcode, byte aa, byte tc, byte rd, byte ra, byte z, byte rcode,
                     byte qdcount,
                     byte ancount,
                     byte nscount,
                     byte arcount) {
        this.id = id;
        this.qr = qr;
        this.opcode = opcode;
        this.aa = aa;
        this.tc = tc;
        this.rd = rd;
        this.ra = ra;
        this.z = z;
        this.rcode = rcode;
        this.qdcount = qdcount;
        this.ancount = ancount;
        this.nscount = nscount;
        this.arcount = arcount;
    }

    public String toBytesString() {
        byte[] bytes = getBytes();
        return HexFormat.of().formatHex(bytes);
    }

    public byte[] getBytes() {
        byte[] bytes = new byte[12];
        bytes[0] = (byte) (id >> 8);
        bytes[1] = (byte) (id & 0x00FF);
        bytes[2] |= qr << 7;
        bytes[2] |= opcode << 3;
        bytes[2] |= aa << 2;
        bytes[2] |= tc << 1;
        bytes[2] |= rd;
        bytes[3] |= ra << 7;
        bytes[3] |= z << 4;
        bytes[3] |= rcode;
        bytes[4] = (byte) (qdcount >> 8);
        bytes[5] = (byte) (qdcount & 0x00FF);
        bytes[6] = (byte) (ancount >> 8);
        bytes[7] = (byte) (ancount & 0x00FF);
        bytes[8] = (byte) (nscount >> 8);
        bytes[9] = (byte) (nscount & 0x00FF);
        bytes[10] = (byte) (arcount >> 8);
        bytes[11] = (byte) (arcount & 0x00FF);
        return bytes;
    }

    @Override
    public String toString() {
        return "DnsHeader [id=" + id + ", qr=" + qr + ", opcode=" + opcode + ", aa=" + aa + ", tc=" + tc + ", rd=" + rd
                + ", ra=" + ra + ", z=" + z + ", rcode=" + rcode + ", qdcount=" + qdcount + ", ancount=" + ancount
                + ", nscount=" + nscount + ", arcount=" + arcount + "]";
    }

    @Override
    public int length() {
        return 12; // fixed length
    }

    public short getId() {
        return id;
    }

    public byte getQr() {
        return qr;
    }

    public byte getOpcode() {
        return opcode;
    }

    public byte getAa() {
        return aa;
    }

    public byte getTc() {
        return tc;
    }

    public byte getRd() {
        return rd;
    }

    public byte getRa() {
        return ra;
    }

    public byte getZ() {
        return z;
    }

    public byte getRcode() {
        return rcode;
    }

    public byte getQdcount() {
        return qdcount;
    }

    public byte getAncount() {
        return ancount;
    }

    public byte getNscount() {
        return nscount;
    }

    public byte getArcount() {
        return arcount;
    }

}
