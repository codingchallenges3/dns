package dns;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HexFormat;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.primitives.Bytes;

public class DnsMessage {
    public static final short MY_ID = 108;
    private static final Logger log = LoggerFactory.getLogger(DnsMessage.class);
    
    private DnsHeader header;
    private DnsQuestion question;
    private DnsResourceRecord[] answers;
    private DnsResourceRecord[] authorities;
    private DnsResourceRecord[] additionals;
    private byte[] data;

    // Create message received
    public DnsMessage(byte[] data) {
        this.data = data;

        int offset = 0;
        this.header = new DnsHeader(data,offset);
        log.debug("Parsed header " + header);
        offset += header.length();
        this.question = new DnsQuestion(data,offset);
        log.debug("Parsed question " + question);
        offset += question.length();
        
        log.debug("Parsing ANSWER records starting at offset " + offset);
        this.answers = parseResourceRecords(data, offset, header.getAncount());
        offset += (Arrays.stream(this.answers).collect(Collectors.summingInt(DnsResourceRecord::length)));

        log.debug("Parsing AUTHORITY records");
        this.authorities = parseResourceRecords(data, offset, header.getNscount());
        offset += (Arrays.stream(this.authorities).collect(Collectors.summingInt(DnsResourceRecord::length)));

        this.additionals = parseResourceRecords(data, offset, header.getArcount());
        offset += (Arrays.stream(this.additionals).collect(Collectors.summingInt(DnsResourceRecord::length)));
    }

    private DnsResourceRecord[] parseResourceRecords(byte[] data, int offset, int count) {
        ArrayList<DnsResourceRecord> list = new ArrayList<DnsResourceRecord>();
        for (int i = 0; i < count; i++) {
            log.debug("Parsing resource record " + i + " of " + count);
            DnsResourceRecord rr = new DnsResourceRecord(this, data, offset);
            list.add(rr);
            offset += rr.length();
        }
        DnsResourceRecord[] rr = new DnsResourceRecord[count];
        return (DnsResourceRecord[]) list.toArray(rr);
    }

    // Create message to send
    public DnsMessage(DnsHeader header, DnsQuestion question, DnsResourceRecord[] answers, DnsResourceRecord[] authorities, DnsResourceRecord[] additionals) {
        this.header = header;
        this.question = question;
        this.answers = answers;
        this.authorities = authorities;
        this.additionals = additionals;
    }

    public byte[] getBytes() {
        return Bytes.concat(header.getBytes(), question.getBytes());
    }

    public String toBytesString() {
        if (data == null) {
            return header.toBytesString() + question.toBytesString() 
                + (answers == null ? "" : "answers.toBytesString()")
                + (authorities == null ? "" : "authorities.toBytesString()")
                + (additionals == null ? "" : "additionals.toBytesString()");
        } else {
            return HexFormat.of().formatHex(data);
        }
    }

    @Override
    public String toString() {
        return "DnsMessage [header=" + header + ", question=" + question + ", answers=" + Arrays.toString(answers)
                + ", authorities=" + Arrays.toString(authorities) + ", additionals=" + Arrays.toString(additionals)
                + "]";
    }

    public static short getMyId() {
        return MY_ID;
    }

    public static Logger getLog() {
        return log;
    }

    public DnsHeader getHeader() {
        return header;
    }

    public DnsQuestion getQuestion() {
        return question;
    }

    public DnsResourceRecord[] getAnswers() {
        return answers;
    }

    public DnsResourceRecord[] getAuthorities() {
        return authorities;
    }

    public DnsResourceRecord[] getAdditionals() {
        return additionals;
    }

    public byte[] getData() {
        return data;
    }

    public boolean hasAnswers() {
        return getAnswers().length > 0;
    }

    public boolean hasAAnswer() {
        return Arrays.stream(answers).filter(answer -> answer.getType() == 1).count() > 0;
    }

    public byte[] getAAnswer() {
        return Arrays.stream(answers).filter(answer -> answer.getType() == 1).findFirst().get().getIpAddress();
    }

    public String getCNAMEAnswer() {
        return Arrays.stream(answers).filter(answer -> answer.getType() == 5).findFirst().get().getDomainName();
    }

    public boolean hasAuthorities() {
        return getAuthorities().length > 0;
    }

    public byte[] getIpAddressForDomainName(String domainName) {
        // Check if the additionals contain an IP address for this domain name
        for (int i = 0; i < additionals.length; i++) {
            DnsResourceRecord dnr = additionals[i];
            if (dnr.getName().equalsIgnoreCase(domainName) && dnr.getIpAddress() != null) {
                return dnr.getIpAddress();
            }
        }
        return null;
    }
}
