package dns;

import java.net.UnknownHostException;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;

public class IncomingDnsResponseHandler extends SimpleChannelInboundHandler<Object> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object data) throws UnknownHostException, InterruptedException {
        ByteBuf buf = ((DatagramPacket) data).content();
        byte[] byteArray = new byte[buf.readableBytes()];

        // Fill the byte array with the data from the buffer
        buf.getBytes(buf.readerIndex(), byteArray);

        DnsMessage message = new DnsMessage(byteArray);
        App.handleIncomingMessage(ctx, message);
    }
}
