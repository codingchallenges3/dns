package dns;

public interface DnsMessagePart {
    public int length();
    public String toBytesString();
}
