package dns;

import java.util.HexFormat;

public class DnsQuestion implements DnsMessagePart {

    private String qname;
    private short qtype;
    private short qclass;

    public DnsQuestion(String qname, short qtype, short qclass) {
        this.qname = qname;
        this.qtype = qtype;
        this.qclass = qclass;
    }

    public DnsQuestion(byte[] bytes, int offset) {
        DecodedName dn = NameDecoder.decode(bytes, offset, bytes);
        this.qname = dn.name();
        int idx = offset + dn.length();
        qtype |= (bytes[idx] << 8);
        qtype |= bytes[idx+1];
        qclass |= (bytes[idx+2] << 8);
        qclass |= bytes[idx+3];
    }

    public String toBytesString() {
        byte[] bytes = getBytes();
        return HexFormat.of().formatHex(bytes);
    }

    public byte[] getBytes() {
        EncodedName encodedQname = encode(qname);
        byte[] bytes = new byte[encodedQname.length() + 4];
        System.arraycopy(encodedQname.name(), 0, bytes, 0, encodedQname.name().length);
        int idx = encodedQname.length();
        bytes[idx] = (byte) (qtype >>> 8);
        bytes[idx+1] = (byte) (qtype & 0x00FF);
        bytes[idx+2] = (byte) (qclass >>> 8);
        bytes[idx+3] = (byte) (qclass & 0x00FF);
        return bytes;
    }

    private EncodedName encode(String qname) {
        String[] split = qname.split("\\.");
        int bytes = split.length;
        for (String part : split) {
            bytes += part.length();
        }
        byte[] result = new byte[bytes+1];
        int idx = 0;
        for (String part : split) {
            result[idx++] = (byte) part.length();
            System.arraycopy(part.getBytes(), 0, result, idx, part.length());
            idx += part.length();
        }
        result[idx] = 0;
        return new EncodedName(result, result.length);
    }

    // Getter methods for each field, if needed
    public String getQname() { return qname; }
    public short getQtype() { return qtype; }
    public short getQclass() { return qclass; }

    @Override
    public String toString() {
        return "DnsQuestion [qname=" + qname + ", qtype=" + qtype + ", qclass=" + qclass + "]";
    }

    @Override
    public int length() {
        return getBytes().length;
    }

}

record EncodedName(byte[] name, int length){};
record DecodedName(String name, int length){};
