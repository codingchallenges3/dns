package dns;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class NameDecoderTest {

    @Test public void testNameDecoderLabels() {
        byte[] test = new byte[] { 3, 'd', 'n', 's', 6, 'g', 'o', 'o', 'g', 'l', 'e', 3, 'c', 'o', 'm', 0};
        DecodedName decode = NameDecoder.decode(test, 0, test);
        assertEquals("dns.google.com", decode.name());
        assertEquals(test.length, decode.length());
    }

    @Test public void testNameDecoderPointer() {
        byte[] test = new byte[] { 3, 'd', 'n', 's', 6, 'g', 'o', 'o', 'g', 'l', 'e', 3, 'c', 'o', 'm', 0, 3, 'w', 'w', 'w', (byte) 0b11000000, (byte) 0b00000100};
        DecodedName decode = NameDecoder.decode(test, 16, test);
        assertEquals("www.google.com", decode.name());
        assertEquals(6, decode.length());
    }
}
