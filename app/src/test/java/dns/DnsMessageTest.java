package dns;

import static org.junit.Assert.assertEquals;

import java.util.HexFormat;

import org.junit.Test;

public class DnsMessageTest {

    @Test public void testEncodeInitialQueryMessage() {
        DnsHeader header = new DnsHeader(DnsMessage.MY_ID, 
            (byte) 0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)0, (byte)0,
            (byte)1, (byte)0, (byte)0, (byte)0);
        DnsQuestion question = new DnsQuestion("dns.google.com", (byte) 1, (byte) 1);
        DnsMessage message = new DnsMessage(header, question, null, null, null);
        assertEquals("Incorrect hex string", "006c01000001000000000000", header.toBytesString());
        assertEquals("Incorrect header length", "006c01000001000000000000".length() / 2, header.length());
        assertEquals("Incorrect hex string", "03646e7306676f6f676c6503636f6d0000010001", question.toBytesString());
        assertEquals("Incorrect question length", "03646e7306676f6f676c6503636f6d0000010001".length() / 2, question.length());
        assertEquals("Incorrect hex string", "006c0100000100000000000003646e7306676f6f676c6503636f6d0000010001", message.toBytesString());
    }

    @Test public void testDecodeHeaderAndQuestion() {
        String response = "006c8180000100020000000003646e7306676f6f676c6503636f6d0000010001c00c0001000100000384000408080404c00c0001000100000384000408080808";
        byte[] data = HexFormat.of().parseHex(response);
        DnsHeader header = new DnsHeader(data, 0);
        assertEquals("Incorrect hex string", "006c81800001000200000000", header.toBytesString());
        DnsQuestion question = new DnsQuestion(data, 12);
        assertEquals("Incorrect qname", "dns.google.com", question.getQname());
        assertEquals("Incorrect qtype", 1, question.getQtype());
        assertEquals("Incorrect qclass", 1, question.getQclass());
    }

    @Test public void testDecodeDnsMessage() {
        String response = "006c8180000100020000000003646e7306676f6f676c6503636f6d0000010001c00c0001000100000384000408080404c00c0001000100000384000408080808";
        byte[] data = HexFormat.of().parseHex(response);
        new DnsMessage(data);
    }

}
